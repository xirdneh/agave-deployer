#--------
# GENERAL
# -------
# Whether to load the dn values of the persistence layer in the auth compose file using --extra-hosts
update_auth_dns: False


# ----
# APIM
# ----
tenant_id: designsafe
agave_env: prod
host: agave.designsafe-ci.org
tenant_admin_role: Internal/designsafe-services-admin

# the url registered in APIM for the profiles API. For tenants using the django service, the
# profiles container will have role: profiles.__tenant_id__.agave.tacc.utexas.edu
# For tenants like ldap, the URL will need to point to a different service (e.g. the PHP service).
agave_profiles_url: profiles.designsafe.agave.tacc.utexas.edu/profiles

deploy_admin_password_grant: True
access_token_validity_time: 14400
deploy_custom_oauth_app: True
update_apim_core_dns: False
apim_increase_global_timeout: False


# -----
# MYSQL
# -----
mysql_host: db.prod.agaveapi.co
# mysql_port: 3301
mysql_port: 3306

# ------------------
# IDENTITY & CLIENTS
# ------------------

# whether or not to deploy the agave_id service container with agaveldap.
use_hosted_id: False

# whether or not to deploy the agave_id service container with a remote userstore.
use_remote_userstore: True

# When true, the services will not make any updates.
agave_id_read_only: True

# unigue id for the "domain name" of the userstore in APIM
remote_id_domain_name: tacc

# URL or service discovery token for the hosted LDAP instance (including port)
remote_ldap_name: ldaps://ldap.tacc.utexas.edu:636
ldap_name: ldaps://ldap.tacc.utexas.edu:636

# account to bind to the LDAP db
remote_auth_ldap_bind_dn: uid=ldapbind,ou=People,dc=tacc,dc=utexas,dc=edu
auth_ldap_bind_dn: uid=ldapbind,ou=People,dc=tacc,dc=utexas,dc=edu

# base search directory for user accounts
remote_ldap_base_search_dn: dc=tacc,dc=utexas,dc=edu
ldap_base_search_dn: dc=tacc,dc=utexas,dc=edu

# Whether or not to check the JWT; When this is False, certain features will not be available such as the
# "me" lookup feature since these features rely on profile information in the JWT.
agave_id_check_jwt: True

# Actual header name that will show up in request.META; value depends on APIM configuration, in particular
# the tenant id specified in api-manager.xml
jwt_header: HTTP_X_JWT_ASSERTION_DESIGNSAFE

# Absolute path to the public key of the APIM instance; used for verifying the signature of the JWT.
agave_id_apim_pub_key: /home/apim/public_keys/apim_default.pub

# APIM Role required to make updates to the LDAP database
agave_id_user_admin_role: Internal/desginsafe-user-account-manager

# Whether or not the USER_ADMIN_ROLE before allowing updates to the LDAP db (/users service)
agave_id_check_user_admin_role: True

# Set USE_CUSTOM_LDAP = True to use a database with a different schema than the traditional Agave ldap (e.g. TACC
# tenant). Some specific fields will still be required, for example the uid field as the primary key.
use_custom_ldap: True

# Base URL of this instance of the service. Used to populate the hyperlinks in the responses.
agave_id_app_base: https://agave.designsafe-ci.org

# DEBUG = True turns up logging and causes Django to generate excpetion pages with stack traces and
# additional information. Should be False in production.
# Updated -- 8/2015: Due to a bug in django, we currently set this to true so that the ALLOWED_HOSTS filtering is
# not activated.
agave_id_debug: True

# Beanstalk connection info
beanstalk_server: 129.114.97.131
beanstalk_port: 11300
beanstalk_tube: prod
beanstalk_srv_code: 0001-001
tenant_uuid: 0001411570898814

# list of additional APIs to automatically subscribe clients to
# agave_clients_additional_apis:

# ----
# CORE
# ----
# http or https, depending on whether auth is deployed in the tacc dc
core_api_protocol: http

# IP of core host. needed until dns gets resolved
core_host: 129.114.97.130

# -----
# HTTPD
# -----
# cert file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
cert_file: designsafe-agave.cer

# cert key file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
cert_key_file: designsafe-agave.key

# add when mounting in a CA cert (not used for self-signed certs) - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
ssl_ca_cert_file: incommon_ca_tacc_utexas_edu.cer


# --------
# HA Proxy
# --------
ha_deployment: True
hap_servers:
    - name: auth1a
      ip: 172.17.0.1
      port: 4080
      ssl_port: 40443
    - name: auth1b
      ip: 172.17.42.1
      port: 4080
      ssl_port: 40443
    - name: auth2a
      ip: 172.17.0.1
      port: 5080
      ssl_port: 50443
    - name: auth2b
      ip: 172.17.42.1
      port: 5080
      ssl_port: 50443
